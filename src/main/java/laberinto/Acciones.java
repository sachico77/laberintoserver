package laberinto;

import java.util.List;

import unlaRedes.LaberintoServer.enums.ComandosEnum;
import unlaRedes.LaberintoServer.enums.ElementosEnum;
import unlaRedes.LaberintoServer.enums.EstadoResponseEnum;
import unlaRedes.LaberintoServer.protocol.CasilleroLaberinto;
import unlaRedes.LaberintoServer.protocol.CasillerosLaberintoList;
import unlaRedes.LaberintoServer.protocol.Estado;
import unlaRedes.LaberintoServer.protocol.Jugador;
import unlaRedes.LaberintoServer.protocol.Laberinto;
import unlaRedes.LaberintoServer.protocol.LaberintoRequest;
import unlaRedes.LaberintoServer.protocol.LaberintoResponse;
import unlaRedes.LaberintoServer.protocol.Login;

public class Acciones {

	/**
	 * Acciones a seguir Segun El comando seleccionado
	 * 
	 * @param laberintoRequest
	 * @return
	 */
	public LaberintoResponse accion(LaberintoRequest laberintoRequest) throws Exception {
		LaberintoResponse laberintoResponse = new LaberintoResponse();
		Estado estado = new Estado();
		
		boolean protocoloValido = this.validarProtocolo(laberintoRequest);
		
		if(!protocoloValido) {
			estado.setCodigoRespues(EstadoResponseEnum.PROTOCOLO_INVALIDO.getCodigo());
			estado.setDescripcion(EstadoResponseEnum.PROTOCOLO_INVALIDO.getDescripcion());
			laberintoResponse.setEstado(estado);
			return laberintoResponse;
		}
		ComandosEnum comandos = ComandosEnum.encontrarValue(laberintoRequest.getComando());

		int filaJugadorNueva = 0;
		int columnaJugadorNueva = 0;
		int posicionJugadorFila = 0;
		int posicionJugadorCol = 0;

		CasillerosLaberintoList fila;
		CasilleroLaberinto casillero;
		Jugador jugador = laberintoRequest.getJugador();
		Laberinto laberinto = laberintoRequest.getLaberinto();
		if (jugador != null) {
			posicionJugadorFila = jugador.getPosicionFila();
			posicionJugadorCol = jugador.getPosicionColumna();
		} else if (!comandos.equals(ComandosEnum.LOGIN)) {
			jugador = new Jugador();
		}

		if (!this.ValidarUsuarioClave(laberintoRequest.getLogin(), estado)) {
			columnaJugadorNueva = posicionJugadorCol;
			filaJugadorNueva = posicionJugadorFila;
			laberintoResponse.setLaberinto(laberinto);
		} else if (comandos == null) {
			filaJugadorNueva = posicionJugadorFila;
			columnaJugadorNueva = posicionJugadorCol;

			estado.setCodigoRespues(EstadoResponseEnum.NO_SE_RECONOCE_COMANDO.getCodigo());
			estado.setDescripcion(EstadoResponseEnum.NO_SE_RECONOCE_COMANDO.getDescripcion());

		} else {
			switch (comandos) {
			case ARRIBA:
				if (posicionJugadorFila == 0) {
					estado.setCodigoRespues(EstadoResponseEnum.MOV_INVALIDO.getCodigo());
					estado.setDescripcion(EstadoResponseEnum.MOV_INVALIDO.getDescripcion());
					filaJugadorNueva = posicionJugadorFila;
					columnaJugadorNueva = posicionJugadorCol;
				} else {
					filaJugadorNueva = posicionJugadorFila - 1;
					columnaJugadorNueva = posicionJugadorCol;

					fila = laberinto.getElementFila().get(filaJugadorNueva);
					casillero = fila.getElementColum().get(columnaJugadorNueva);

					if (casillero.isPuedePasar()
							|| (jugador.isPasaGuarda()
									&& casillero.getElemento().equalsIgnoreCase(ElementosEnum.GUARDIA.getElemento()))
							|| (jugador.isPoseeKey()
									&& casillero.getElemento().equalsIgnoreCase(ElementosEnum.SALIDA.getElemento()))) {
						this.obtenerElementos(laberintoRequest, casillero);
						this.mostrarArriba(filaJugadorNueva, columnaJugadorNueva, laberinto);
					} else if (casillero.getElemento().equalsIgnoreCase(ElementosEnum.GUARDIA.getElemento())) {

						estado.setCodigoRespues(EstadoResponseEnum.ATRAPADO.getCodigo());
						estado.setDescripcion(EstadoResponseEnum.ATRAPADO.getDescripcion());
						laberintoRequest.getLaberinto().setFinalizado(true);

					} else if (casillero.getElemento().equalsIgnoreCase(ElementosEnum.SALIDA.getElemento())
							&& !laberintoRequest.getJugador().isPoseeKey()) {

						filaJugadorNueva = posicionJugadorFila;
						estado.setCodigoRespues(EstadoResponseEnum.FALTA_LLAVE.getCodigo());
						estado.setDescripcion(EstadoResponseEnum.FALTA_LLAVE.getDescripcion());
					} else {
						filaJugadorNueva = posicionJugadorFila;
						estado.setCodigoRespues(EstadoResponseEnum.MOV_INVALIDO.getCodigo());
						estado.setDescripcion(EstadoResponseEnum.MOV_INVALIDO.getDescripcion());
					}
				}

				laberintoResponse.setLaberinto(laberinto);
				break;
			case ABAJO:
				if (posicionJugadorFila == laberinto.getElementFila().size()) {
					estado = new Estado();
					estado.setCodigoRespues(EstadoResponseEnum.MOV_INVALIDO.getCodigo());
					estado.setDescripcion(EstadoResponseEnum.MOV_INVALIDO.getDescripcion());
					filaJugadorNueva = posicionJugadorFila;
					columnaJugadorNueva = posicionJugadorCol;
				} else {
					filaJugadorNueva = posicionJugadorFila + 1;
					columnaJugadorNueva = posicionJugadorCol;

					fila = laberinto.getElementFila().get(filaJugadorNueva);
					casillero = fila.getElementColum().get(columnaJugadorNueva);

					if (casillero.isPuedePasar()
							|| (jugador.isPasaGuarda()
									&& casillero.getElemento().equalsIgnoreCase(ElementosEnum.GUARDIA.getElemento()))
							|| (jugador.isPoseeKey()
									&& casillero.getElemento().equalsIgnoreCase(ElementosEnum.SALIDA.getElemento()))) {
						this.obtenerElementos(laberintoRequest, casillero);
						this.mostrarAbajo(filaJugadorNueva, columnaJugadorNueva, laberinto);
					} else if (casillero.getElemento().equalsIgnoreCase(ElementosEnum.GUARDIA.getElemento())) {

						estado.setCodigoRespues(EstadoResponseEnum.ATRAPADO.getCodigo());
						estado.setDescripcion(EstadoResponseEnum.ATRAPADO.getDescripcion());
						laberintoRequest.getLaberinto().setFinalizado(true);

					} else if (casillero.getElemento().equalsIgnoreCase(ElementosEnum.SALIDA.getElemento())
							&& !laberintoRequest.getJugador().isPoseeKey()) {

						filaJugadorNueva = posicionJugadorFila;
						estado.setCodigoRespues(EstadoResponseEnum.FALTA_LLAVE.getCodigo());
						estado.setDescripcion(EstadoResponseEnum.FALTA_LLAVE.getDescripcion());
					} else {
						filaJugadorNueva = posicionJugadorFila;
						estado.setCodigoRespues(EstadoResponseEnum.MOV_INVALIDO.getCodigo());
						estado.setDescripcion(EstadoResponseEnum.MOV_INVALIDO.getDescripcion());
					}
				}

				laberintoResponse.setLaberinto(laberinto);
				break;
			case IZQUIERDA:

				if (posicionJugadorCol == 0) {
					estado.setCodigoRespues(EstadoResponseEnum.MOV_INVALIDO.getCodigo());
					estado.setDescripcion(EstadoResponseEnum.MOV_INVALIDO.getDescripcion());
					filaJugadorNueva = posicionJugadorFila;
					columnaJugadorNueva = posicionJugadorCol;
				} else {
					filaJugadorNueva = posicionJugadorFila;
					columnaJugadorNueva = posicionJugadorCol - 1;

					fila = laberinto.getElementFila().get(filaJugadorNueva);
					casillero = fila.getElementColum().get(columnaJugadorNueva);

					if (casillero.isPuedePasar()
							|| (jugador.isPasaGuarda()
									&& casillero.getElemento().equalsIgnoreCase(ElementosEnum.GUARDIA.getElemento()))
							|| (jugador.isPoseeKey()
									&& casillero.getElemento().equalsIgnoreCase(ElementosEnum.SALIDA.getElemento()))) {
						this.obtenerElementos(laberintoRequest, casillero);
						this.mostrarIzquierda(filaJugadorNueva, columnaJugadorNueva, laberinto);
					} else if (casillero.getElemento().equalsIgnoreCase(ElementosEnum.GUARDIA.getElemento())) {

						estado.setCodigoRespues(EstadoResponseEnum.ATRAPADO.getCodigo());
						estado.setDescripcion(EstadoResponseEnum.ATRAPADO.getDescripcion());
						laberintoRequest.getLaberinto().setFinalizado(true);

					} else if (casillero.getElemento().equalsIgnoreCase(ElementosEnum.SALIDA.getElemento())
							&& !laberintoRequest.getJugador().isPoseeKey()) {

						columnaJugadorNueva = posicionJugadorCol;
						estado.setCodigoRespues(EstadoResponseEnum.FALTA_LLAVE.getCodigo());
						estado.setDescripcion(EstadoResponseEnum.FALTA_LLAVE.getDescripcion());
					} else {
						columnaJugadorNueva = posicionJugadorCol;
						estado.setCodigoRespues(EstadoResponseEnum.MOV_INVALIDO.getCodigo());
						estado.setDescripcion(EstadoResponseEnum.MOV_INVALIDO.getDescripcion());
					}
				}

				laberintoResponse.setLaberinto(laberinto);
				break;
			case DERECHA:
				if (posicionJugadorCol == laberinto.getElementFila().get(posicionJugadorFila).getElementColum()
						.size()) {

					estado.setCodigoRespues(EstadoResponseEnum.MOV_INVALIDO.getCodigo());
					estado.setDescripcion(EstadoResponseEnum.MOV_INVALIDO.getDescripcion());
					filaJugadorNueva = posicionJugadorFila;
					columnaJugadorNueva = posicionJugadorCol;
				} else {
					filaJugadorNueva = posicionJugadorFila;
					columnaJugadorNueva = posicionJugadorCol + 1;

					fila = laberinto.getElementFila().get(filaJugadorNueva);
					casillero = fila.getElementColum().get(columnaJugadorNueva);

					if (casillero.isPuedePasar()
							|| (jugador.isPasaGuarda()
									&& casillero.getElemento().equalsIgnoreCase(ElementosEnum.GUARDIA.getElemento()))
							|| (jugador.isPoseeKey()
									&& casillero.getElemento().equalsIgnoreCase(ElementosEnum.SALIDA.getElemento()))) {
						this.obtenerElementos(laberintoRequest, casillero);
						this.mostrarDerecha(filaJugadorNueva, columnaJugadorNueva, laberinto, posicionJugadorFila);
					} else if (casillero.getElemento().equalsIgnoreCase(ElementosEnum.GUARDIA.getElemento())) {

						estado.setCodigoRespues(EstadoResponseEnum.ATRAPADO.getCodigo());
						estado.setDescripcion(EstadoResponseEnum.ATRAPADO.getDescripcion());
						laberintoRequest.getLaberinto().setFinalizado(true);

					} else if (casillero.getElemento().equalsIgnoreCase(ElementosEnum.SALIDA.getElemento())
							&& !laberintoRequest.getJugador().isPoseeKey()) {

						columnaJugadorNueva = posicionJugadorCol;
						estado.setCodigoRespues(EstadoResponseEnum.FALTA_LLAVE.getCodigo());
						estado.setDescripcion(EstadoResponseEnum.FALTA_LLAVE.getDescripcion());
					} else {
						columnaJugadorNueva = posicionJugadorCol;
						estado.setCodigoRespues(EstadoResponseEnum.MOV_INVALIDO.getCodigo());
						estado.setDescripcion(EstadoResponseEnum.MOV_INVALIDO.getDescripcion());
					}
				}

				laberintoResponse.setLaberinto(laberinto);
				break;
			case INICIARLAB:
				boolean encontrado = false;
				int contaColumnaFor = 0;
				int contaFilaFor = 0;

				laberinto = this.SeleccionarEscenario(laberinto.getNumero());
				
				if (laberinto == null) {
					estado.setCodigoRespues(EstadoResponseEnum.NO_EXISTE_LAB.getCodigo());
					estado.setDescripcion(EstadoResponseEnum.NO_EXISTE_LAB.getDescripcion());
					break;
				}

				List<CasillerosLaberintoList> filas = laberinto.getElementFila();
				for (CasillerosLaberintoList filaFor : filas) {
					for (CasilleroLaberinto columnaFor : filaFor.getElementColum()) {
						if (columnaFor.getElemento().equalsIgnoreCase(ElementosEnum.ENTRADA.getElemento())) {
							columnaJugadorNueva = contaColumnaFor;
							filaJugadorNueva = contaFilaFor;
							encontrado = true;
							break;
						}
						contaColumnaFor++;
					}
					if (encontrado) {
						break;
					}
					contaColumnaFor = 0;
					contaFilaFor++;
				}

				laberinto.getElementFila().get(filaJugadorNueva).getElementColum().get(columnaJugadorNueva)
						.setMostrar(true);
				this.mostrarAbajo(filaJugadorNueva, columnaJugadorNueva, laberinto);
				this.mostrarArriba(filaJugadorNueva, columnaJugadorNueva, laberinto);
				this.mostrarDerecha(filaJugadorNueva, columnaJugadorNueva, laberinto, filaJugadorNueva);
				this.mostrarIzquierda(filaJugadorNueva, columnaJugadorNueva, laberinto);

				laberintoResponse.setLaberinto(laberinto);

				break;
			case LOGIN:
				estado.setCodigoRespues(EstadoResponseEnum.LOG_EXITOS.getCodigo());
				estado.setDescripcion(EstadoResponseEnum.LOG_EXITOS.getDescripcion());
				break;
			default:
				estado.setCodigoRespues(EstadoResponseEnum.NO_SE_RECONOCE_COMANDO.getCodigo());
				estado.setDescripcion(EstadoResponseEnum.NO_SE_RECONOCE_COMANDO.getDescripcion());

				break;

			}
		}

		if (jugador != null) {
			jugador.setPosicionColumna(columnaJugadorNueva);
			jugador.setPosicionFila(filaJugadorNueva);
			laberintoResponse.setJugador(jugador);

		}

		laberintoResponse.setEstado(estado);

		laberintoResponse.setLaberinto(laberinto);

		laberintoResponse.setLogin(laberintoRequest.getLogin());

		if (estado.getCodigoRespues() == null || estado.getCodigoRespues().isEmpty()) {
			estado.setCodigoRespues(EstadoResponseEnum.Exito.getCodigo());
			estado.setDescripcion(EstadoResponseEnum.Exito.getDescripcion());
		}
		return laberintoResponse;

	}

	/***
	 * Seleccion de laberinto
	 * 
	 * @param numero
	 * @return
	 */
	private Laberinto SeleccionarEscenario(int numero) {
		Laberinto laberinto = null;
		Escenarios esc = new Escenarios();
		switch (numero) {
		case 1:

			laberinto = esc.escenarioUno();
			laberinto.setNumero(numero);
			break;
		case 2:
			laberinto = esc.escenarioDos();
			laberinto.setNumero(numero);
			break;
		case 3:
			laberinto = esc.escenarioTres();
			laberinto.setNumero(numero);
			break;
		case 4:
			laberinto = esc.escenarioCuatro();
			laberinto.setNumero(numero);
			break;
		case 5:
			laberinto = esc.escenarioCinco();
			laberinto.setNumero(numero);
			break;
		default:
			// Aca habria que eviar un msj al cliente de que ese escribio cualquier cosa
			break;
		}

		return laberinto;
	}

	/***
	 * Logica para mostrar al moverse un casillero a la izquierda
	 * 
	 * @param fila
	 * @param columna
	 * @param laberinto
	 */
	private void mostrarIzquierda(int fila, int columna, Laberinto laberinto) {
		int contColumna = 1;
		int auxCol = columna;

		while (contColumna <= 2) {
			auxCol--;
			if (auxCol >= 0) {
				this.mostrarCasillerosHorizontal(auxCol, fila, laberinto);
			} else {
				break;
			}
			contColumna++;
		}
	}

	/***
	 * Logica para mostrar al correrse un casillero a la derecha
	 * 
	 * @param fila
	 * @param columna
	 * @param laberinto
	 * @param posicionJugadorFila
	 */
	private void mostrarDerecha(int fila, int columna, Laberinto laberinto, int posicionJugadorFila) {
		int contColumna = 1;
		int auxCol = columna;

		while (contColumna <= 2) {
			auxCol++;
			if (auxCol < laberinto.getElementFila().get(posicionJugadorFila).getElementColum().size()) {
				this.mostrarCasillerosHorizontal(auxCol, fila, laberinto);
			} else {
				break;
			}
			contColumna++;
		}
	}

	/**
	 * Logica para Mostrar al subir un casillero
	 * 
	 * @param fila
	 * @param columna
	 * @param laberinto
	 */
	private void mostrarArriba(int fila, int columna, Laberinto laberinto) {

		int auxFila = fila;
		int contFila = 1;

		while (contFila <= 2) {
			auxFila--;
			if (auxFila >= 0) {
				this.mostrarCasillerosVertical(auxFila, columna, laberinto);
			} else {
				break;
			}
			contFila++;
		}
	}

	/**
	 * Logica para MOSTRAR al bajar un casillero
	 * 
	 * @param fila
	 * @param columna
	 * @param laberinto
	 */
	private void mostrarAbajo(int fila, int columna, Laberinto laberinto) {
		int auxFila = fila;
		int contFila = 1;

		while (contFila <= 2) {
			auxFila++;
			if (auxFila < laberinto.getElementFila().size()) {
				this.mostrarCasillerosVertical(auxFila, columna, laberinto);
			} else {
				break;
			}
			contFila++;
		}
	}

	/***
	 * Muestra los casilleros verticales a medida que avanza
	 * 
	 * @param fila
	 * @param columna
	 * @param laberinto
	 */
	private void mostrarCasillerosVertical(int fila, int columna, Laberinto laberinto) {

		int auxColumnaIzq = columna;
		int auxColumnaDer = columna;
		CasillerosLaberintoList filaMostrar;
		CasilleroLaberinto casilleroMostrar;
		int contColumna = 1;
		filaMostrar = laberinto.getElementFila().get(fila);
		casilleroMostrar = filaMostrar.getElementColum().get(columna);

		casilleroMostrar.setMostrar(true);

		while (contColumna <= 2) {
			auxColumnaDer++;
			auxColumnaIzq--;

			filaMostrar = laberinto.getElementFila().get(fila);
			// if(auxColumnaDer!=10){
			if (auxColumnaDer < filaMostrar.getElementColum().size()) {
				casilleroMostrar = filaMostrar.getElementColum().get(auxColumnaDer);

				casilleroMostrar.setMostrar(true);
			}

			if (auxColumnaIzq >= 0) {
				casilleroMostrar = filaMostrar.getElementColum().get(auxColumnaIzq);
				casilleroMostrar.setMostrar(true);
			}
			// }
			contColumna++;
		}
	}

	/**
	 * Muestra los casillero Horizontales a medida que avanza
	 * 
	 * @param columna
	 * @param fila
	 * @param laberinto
	 */
	private void mostrarCasillerosHorizontal(int columna, int fila, Laberinto laberinto) {

		int auxFilaArr = fila;
		int auxFilaAb = fila;
		CasillerosLaberintoList filaMostrar;
		CasilleroLaberinto casilleroMostrar;
		int contFila = 1;
		filaMostrar = laberinto.getElementFila().get(fila);
		casilleroMostrar = filaMostrar.getElementColum().get(columna);
		casilleroMostrar.setMostrar(true);

		while (contFila <= 2) {
			auxFilaArr--;
			auxFilaAb++;

			if (auxFilaAb < laberinto.getElementFila().size()) {
				filaMostrar = laberinto.getElementFila().get(auxFilaAb);
				casilleroMostrar = filaMostrar.getElementColum().get(columna);
				casilleroMostrar.setMostrar(true);
			}

			if (auxFilaArr >= 0) {
				filaMostrar = laberinto.getElementFila().get(auxFilaArr);
				casilleroMostrar = filaMostrar.getElementColum().get(columna);
				casilleroMostrar.setMostrar(true);
			}

			contFila++;
		}
	}

	/**
	 * Obtienen Los elementos que se van encontrando en el laberinto y valida los
	 * guardias y la salida
	 * 
	 * @param laberinto
	 * @param casillero
	 */
	private void obtenerElementos(LaberintoRequest laberinto, CasilleroLaberinto casillero) {

		if (casillero.getElemento().equalsIgnoreCase(ElementosEnum.ORO.getElemento())) {
			laberinto.getJugador().setOro(laberinto.getJugador().getOro() + 1);
			casillero.setElemento(ElementosEnum.CAMINO.getElemento().toLowerCase());
			laberinto.getJugador().setPasaGuarda(true);
		}

		if (casillero.getElemento().equalsIgnoreCase(ElementosEnum.LLAVE.getElemento())) {
			casillero.setElemento(ElementosEnum.CAMINO.getElemento().toLowerCase());
			laberinto.getJugador().setPoseeKey(true);
		}

		if (casillero.getElemento().equalsIgnoreCase(ElementosEnum.SALIDA.getElemento())
				&& laberinto.getJugador().isPoseeKey()) {
			laberinto.getJugador().setPoseeKey(false);
			laberinto.getLaberinto().setFinalizado(true);
		}

		if (casillero.getElemento().equalsIgnoreCase(ElementosEnum.GUARDIA.getElemento())) {
			laberinto.getJugador().setOro(laberinto.getJugador().getOro() - 1);
			casillero.setPuedePasar(true);
			casillero.setElemento(ElementosEnum.CAMINO.getElemento().toLowerCase());
			if (laberinto.getJugador().getOro() == 0) {
				laberinto.getJugador().setPasaGuarda(false);

			}
		}
	}

	/***
	 * VAlida si el usuario y clave son correctos
	 * 
	 * @param login
	 * @param estado
	 * @return
	 */
	private boolean ValidarUsuarioClave(Login login, Estado estado) {
		boolean valido = true;
		if ("123".compareTo(login.getClave()) != 0) {
			estado.setDescripcion(EstadoResponseEnum.CONTRASENA_INVALIDA.getDescripcion());
			estado.setCodigoRespues(EstadoResponseEnum.CONTRASENA_INVALIDA.getCodigo());
			valido = false;
		}

		if ("redes".compareTo(login.getUsuario()) != 0) {
			estado.setDescripcion(EstadoResponseEnum.USUARIO_INVALIDO.getDescripcion());
			estado.setCodigoRespues(EstadoResponseEnum.USUARIO_INVALIDO.getCodigo());
			valido = false;
		}

		return valido;
	}

	private boolean validarProtocolo (LaberintoRequest laberintoRequest) {
	 
	 boolean valido = true;

	 if( laberintoRequest.getComando() == null 
		 || laberintoRequest.getLogin() == null
		 || (!laberintoRequest.getComando().equals(ComandosEnum.LOGIN.getComando()) && laberintoRequest.getLaberinto() == null)) {
		 valido = false;
	 }
		 
	 return valido;

	 }
}
