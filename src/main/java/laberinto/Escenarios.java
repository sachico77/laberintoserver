package laberinto;

import java.util.ArrayList;
import java.util.List;

import unlaRedes.LaberintoServer.protocol.CasilleroLaberinto;
import unlaRedes.LaberintoServer.protocol.CasillerosLaberintoList;
import unlaRedes.LaberintoServer.protocol.Laberinto;

public class Escenarios{
	
	public Laberinto escenarioUno(){
		
		Laberinto laberinto = new Laberinto();
		
		CasillerosLaberintoList columna1 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna2 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna3 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna4 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna5 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna6 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna7 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna8 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna9 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna10 = new CasillerosLaberintoList();
		
		List<CasillerosLaberintoList> filas = new ArrayList<CasillerosLaberintoList>();
		
		
		List<CasilleroLaberinto> lista1 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista2 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista3 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista4 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista5 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista6 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista7 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista8 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista9 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista10 = new ArrayList<CasilleroLaberinto>();
		
		CasilleroLaberinto casillero1 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero2 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero3 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero4 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero5 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero6 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero7 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero8 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero9 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero10 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero11 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero12 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero13 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero14 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero15 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero16 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero17 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero18 = new CasilleroLaberinto("k",false,true);
		CasilleroLaberinto casillero19 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero20 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero21 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero22 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero23 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero24 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero25 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero26 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero27 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero28 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero29 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero30 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero31 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero32 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero33 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero34 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero35 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero36 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero37 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero38 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero39 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero40 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero41 = new CasilleroLaberinto("e",true,true);
		CasilleroLaberinto casillero42 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero43 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero44 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero45 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero46 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero47 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero48 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero49 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero50 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero51 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero52 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero53 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero54 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero55 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero56 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero57 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero58 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero59 = new CasilleroLaberinto("g",false,false);
		CasilleroLaberinto casillero60 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero61 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero62 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero63 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero64 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero65 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero66 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero67 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero68 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero69 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero70 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero71 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero72 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero73 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero74 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero75 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero76 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero77 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero78 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero79 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero80 = new CasilleroLaberinto("s",false,false);
		
		CasilleroLaberinto casillero81 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero82 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero83 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero84 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero85 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero86 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero87 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero88 = new CasilleroLaberinto("g",false,false);
		CasilleroLaberinto casillero89 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero90 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero91 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero92 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero93 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero94 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero95 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero96 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero97 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero98 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero99 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero100 = new CasilleroLaberinto("p",false,false);
		
		lista1.add(casillero1);
		lista1.add(casillero2);
		lista1.add(casillero3);
		lista1.add(casillero4);
		lista1.add(casillero5);
		lista1.add(casillero6);
		lista1.add(casillero7);
		lista1.add(casillero8);
		lista1.add(casillero9);
		lista1.add(casillero10);
		
		columna1.setElementColum(lista1);
	
		
		lista2.add(casillero11);
		lista2.add(casillero12);
		lista2.add(casillero13);
		lista2.add(casillero14);
		lista2.add(casillero15);
		lista2.add(casillero16);
		lista2.add(casillero17);
		lista2.add(casillero18);
		lista2.add(casillero19);
		lista2.add(casillero20);
		
		columna2.setElementColum(lista2);
		
		lista3.add(casillero21);
		lista3.add(casillero22);
		lista3.add(casillero23);
		lista3.add(casillero24);
		lista3.add(casillero25);
		lista3.add(casillero26);
		lista3.add(casillero27);
		lista3.add(casillero28);
		lista3.add(casillero29);
		lista3.add(casillero30);
		
		columna3.setElementColum(lista3);
		
		lista4.add(casillero31);
		lista4.add(casillero32);
		lista4.add(casillero33);
		lista4.add(casillero34);
		lista4.add(casillero35);
		lista4.add(casillero36);
		lista4.add(casillero37);
		lista4.add(casillero38);
		lista4.add(casillero39);
		lista4.add(casillero40);
		
		columna4.setElementColum(lista4);
		
		lista5.add(casillero41);
		lista5.add(casillero42);
		lista5.add(casillero43);
		lista5.add(casillero44);
		lista5.add(casillero45);
		lista5.add(casillero46);
		lista5.add(casillero47);
		lista5.add(casillero48);
		lista5.add(casillero49);
		lista5.add(casillero50);
		
		columna5.setElementColum(lista5);
		
		lista6.add(casillero51);
		lista6.add(casillero52);
		lista6.add(casillero53);
		lista6.add(casillero54);
		lista6.add(casillero55);
		lista6.add(casillero56);
		lista6.add(casillero57);
		lista6.add(casillero58);
		lista6.add(casillero59);
		lista6.add(casillero60);
		
		columna6.setElementColum(lista6);
		
		lista7.add(casillero61);
		lista7.add(casillero62);
		lista7.add(casillero63);
		lista7.add(casillero64);
		lista7.add(casillero65);
		lista7.add(casillero66);
		lista7.add(casillero67);
		lista7.add(casillero68);
		lista7.add(casillero69);
		lista7.add(casillero70);
		
		columna7.setElementColum(lista7);
		
		lista8.add(casillero71);
		lista8.add(casillero72);
		lista8.add(casillero73);
		lista8.add(casillero74);
		lista8.add(casillero75);
		lista8.add(casillero76);
		lista8.add(casillero77);
		lista8.add(casillero78);
		lista8.add(casillero79);
		lista8.add(casillero80);
		
		columna8.setElementColum(lista8);
		
		lista9.add(casillero81);
		lista9.add(casillero82);
		lista9.add(casillero83);
		lista9.add(casillero84);
		lista9.add(casillero85);
		lista9.add(casillero86);
		lista9.add(casillero87);
		lista9.add(casillero88);
		lista9.add(casillero89);
		lista9.add(casillero90);
		
		columna9.setElementColum(lista9);
		
		lista10.add(casillero91);
		lista10.add(casillero92);
		lista10.add(casillero93);
		lista10.add(casillero94);
		lista10.add(casillero95);
		lista10.add(casillero96);
		lista10.add(casillero97);
		lista10.add(casillero98);
		lista10.add(casillero99);
		lista10.add(casillero100);
		
		columna10.setElementColum(lista10);
		
		filas.add(columna1);
		filas.add(columna2);
		filas.add(columna3);
		filas.add(columna4);
		filas.add(columna5);
		filas.add(columna6);
		filas.add(columna7);
		filas.add(columna8);
		filas.add(columna9);
		filas.add(columna10);
		
		laberinto.setElementFila(filas);

		return laberinto;
	}
	
	public Laberinto escenarioDos(){

		Laberinto laberinto = new Laberinto();
		
		CasillerosLaberintoList columna1 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna2 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna3 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna4 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna5 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna6 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna7 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna8 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna9 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna10 = new CasillerosLaberintoList();
		
		List<CasillerosLaberintoList> filas = new ArrayList<CasillerosLaberintoList>();
		
		
		List<CasilleroLaberinto> lista1 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista2 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista3 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista4 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista5 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista6 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista7 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista8 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista9 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista10 = new ArrayList<CasilleroLaberinto>();
		
		CasilleroLaberinto casillero1 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero2 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero3 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero4 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero5 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero6 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero7 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero8 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero9 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero10 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero11 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero12 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero13 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero14 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero15 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero16 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero17 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero18 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero19 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero20 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero21 = new CasilleroLaberinto("e",true,true);
		CasilleroLaberinto casillero22 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero23 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero24 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero25 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero26 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero27 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero28 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero29 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero30 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero31 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero32 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero33 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero34 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero35 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero36 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero37 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero38 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero39 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero40 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero41 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero42 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero43 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero44 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero45 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero46 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero47 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero48 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero49 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero50 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero51 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero52 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero53 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero54 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero55 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero56 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero57 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero58 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero59 = new CasilleroLaberinto("g",false,false);
		CasilleroLaberinto casillero60 = new CasilleroLaberinto("s",false,false);
		
		CasilleroLaberinto casillero61 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero62 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero63 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero64 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero65 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero66 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero67 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero68 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero69 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero70 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero71 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero72 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero73 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero74 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero75 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero76 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero77 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero78 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero79 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero80 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero81 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero82 = new CasilleroLaberinto("k",false,true);
		CasilleroLaberinto casillero83 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero84 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero85 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero86 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero87 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero88 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero89 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero90 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero91 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero92 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero93 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero94 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero95 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero96 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero97 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero98 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero99 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero100 = new CasilleroLaberinto("p",false,false);
		
		lista1.add(casillero1);
		lista1.add(casillero2);
		lista1.add(casillero3);
		lista1.add(casillero4);
		lista1.add(casillero5);
		lista1.add(casillero6);
		lista1.add(casillero7);
		lista1.add(casillero8);
		lista1.add(casillero9);
		lista1.add(casillero10);
		
		columna1.setElementColum(lista1);
	
		
		lista2.add(casillero11);
		lista2.add(casillero12);
		lista2.add(casillero13);
		lista2.add(casillero14);
		lista2.add(casillero15);
		lista2.add(casillero16);
		lista2.add(casillero17);
		lista2.add(casillero18);
		lista2.add(casillero19);
		lista2.add(casillero20);
		
		columna2.setElementColum(lista2);
		
		lista3.add(casillero21);
		lista3.add(casillero22);
		lista3.add(casillero23);
		lista3.add(casillero24);
		lista3.add(casillero25);
		lista3.add(casillero26);
		lista3.add(casillero27);
		lista3.add(casillero28);
		lista3.add(casillero29);
		lista3.add(casillero30);
		
		columna3.setElementColum(lista3);
		
		lista4.add(casillero31);
		lista4.add(casillero32);
		lista4.add(casillero33);
		lista4.add(casillero34);
		lista4.add(casillero35);
		lista4.add(casillero36);
		lista4.add(casillero37);
		lista4.add(casillero38);
		lista4.add(casillero39);
		lista4.add(casillero40);
		
		columna4.setElementColum(lista4);
		
		lista5.add(casillero41);
		lista5.add(casillero42);
		lista5.add(casillero43);
		lista5.add(casillero44);
		lista5.add(casillero45);
		lista5.add(casillero46);
		lista5.add(casillero47);
		lista5.add(casillero48);
		lista5.add(casillero49);
		lista5.add(casillero50);
		
		columna5.setElementColum(lista5);
		
		lista6.add(casillero51);
		lista6.add(casillero52);
		lista6.add(casillero53);
		lista6.add(casillero54);
		lista6.add(casillero55);
		lista6.add(casillero56);
		lista6.add(casillero57);
		lista6.add(casillero58);
		lista6.add(casillero59);
		lista6.add(casillero60);
		
		columna6.setElementColum(lista6);
		
		lista7.add(casillero61);
		lista7.add(casillero62);
		lista7.add(casillero63);
		lista7.add(casillero64);
		lista7.add(casillero65);
		lista7.add(casillero66);
		lista7.add(casillero67);
		lista7.add(casillero68);
		lista7.add(casillero69);
		lista7.add(casillero70);
		
		columna7.setElementColum(lista7);
		
		lista8.add(casillero71);
		lista8.add(casillero72);
		lista8.add(casillero73);
		lista8.add(casillero74);
		lista8.add(casillero75);
		lista8.add(casillero76);
		lista8.add(casillero77);
		lista8.add(casillero78);
		lista8.add(casillero79);
		lista8.add(casillero80);
		
		columna8.setElementColum(lista8);
		
		lista9.add(casillero81);
		lista9.add(casillero82);
		lista9.add(casillero83);
		lista9.add(casillero84);
		lista9.add(casillero85);
		lista9.add(casillero86);
		lista9.add(casillero87);
		lista9.add(casillero88);
		lista9.add(casillero89);
		lista9.add(casillero90);
		
		columna9.setElementColum(lista9);
		
		lista10.add(casillero91);
		lista10.add(casillero92);
		lista10.add(casillero93);
		lista10.add(casillero94);
		lista10.add(casillero95);
		lista10.add(casillero96);
		lista10.add(casillero97);
		lista10.add(casillero98);
		lista10.add(casillero99);
		lista10.add(casillero100);
		
		columna10.setElementColum(lista10);
		
		filas.add(columna1);
		filas.add(columna2);
		filas.add(columna3);
		filas.add(columna4);
		filas.add(columna5);
		filas.add(columna6);
		filas.add(columna7);
		filas.add(columna8);
		filas.add(columna9);
		filas.add(columna10);
		
		laberinto.setElementFila(filas);

		return laberinto;
		
	}
	
	public Laberinto escenarioTres(){
		
		Laberinto laberinto = new Laberinto();
		
		CasillerosLaberintoList columna1 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna2 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna3 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna4 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna5 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna6 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna7 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna8 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna9 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna10 = new CasillerosLaberintoList();
		
		List<CasillerosLaberintoList> filas = new ArrayList<CasillerosLaberintoList>();
		
		
		List<CasilleroLaberinto> lista1 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista2 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista3 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista4 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista5 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista6 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista7 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista8 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista9 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista10 = new ArrayList<CasilleroLaberinto>();
		
		CasilleroLaberinto casillero1 = new CasilleroLaberinto("e",true,false);
		CasilleroLaberinto casillero2 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero3 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero4 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero5 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero6 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero7 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero8 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero9 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero10 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero11 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero12 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero13 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero14 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero15 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero16 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero17 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero18 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero19 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero20 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero21 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero22 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero23 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero24 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero25 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero26 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero27 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero28 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero29 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero30 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero31 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero32 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero33 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero34 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero35 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero36 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero37 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero38 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero39 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero40 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero41 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero42 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero43 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero44 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero45 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero46 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero47 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero48 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero49 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero50 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero51 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero52 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero53 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero54 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero55 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero56 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero57 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero58 = new CasilleroLaberinto("k",false,true);
		CasilleroLaberinto casillero59 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero60 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero61 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero62 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero63 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero64 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero65 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero66 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero67 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero68 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero69 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero70 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero71 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero72 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero73 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero74 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero75 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero76 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero77 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero78 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero79 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero80 = new CasilleroLaberinto("s",false,false);
		
		CasilleroLaberinto casillero81 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero82 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero83 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero84 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero85 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero86 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero87 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero88 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero89 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero90 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero91 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero92 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero93 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero94 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero95 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero96 = new CasilleroLaberinto("g",false,false);
		CasilleroLaberinto casillero97 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero98 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero99 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero100 = new CasilleroLaberinto("p",false,false);
		
		lista1.add(casillero1);
		lista1.add(casillero2);
		lista1.add(casillero3);
		lista1.add(casillero4);
		lista1.add(casillero5);
		lista1.add(casillero6);
		lista1.add(casillero7);
		lista1.add(casillero8);
		lista1.add(casillero9);
		lista1.add(casillero10);
		
		columna1.setElementColum(lista1);
	
		
		lista2.add(casillero11);
		lista2.add(casillero12);
		lista2.add(casillero13);
		lista2.add(casillero14);
		lista2.add(casillero15);
		lista2.add(casillero16);
		lista2.add(casillero17);
		lista2.add(casillero18);
		lista2.add(casillero19);
		lista2.add(casillero20);
		
		columna2.setElementColum(lista2);
		
		lista3.add(casillero21);
		lista3.add(casillero22);
		lista3.add(casillero23);
		lista3.add(casillero24);
		lista3.add(casillero25);
		lista3.add(casillero26);
		lista3.add(casillero27);
		lista3.add(casillero28);
		lista3.add(casillero29);
		lista3.add(casillero30);
		
		columna3.setElementColum(lista3);
		
		lista4.add(casillero31);
		lista4.add(casillero32);
		lista4.add(casillero33);
		lista4.add(casillero34);
		lista4.add(casillero35);
		lista4.add(casillero36);
		lista4.add(casillero37);
		lista4.add(casillero38);
		lista4.add(casillero39);
		lista4.add(casillero40);
		
		columna4.setElementColum(lista4);
		
		lista5.add(casillero41);
		lista5.add(casillero42);
		lista5.add(casillero43);
		lista5.add(casillero44);
		lista5.add(casillero45);
		lista5.add(casillero46);
		lista5.add(casillero47);
		lista5.add(casillero48);
		lista5.add(casillero49);
		lista5.add(casillero50);
		
		columna5.setElementColum(lista5);
		
		lista6.add(casillero51);
		lista6.add(casillero52);
		lista6.add(casillero53);
		lista6.add(casillero54);
		lista6.add(casillero55);
		lista6.add(casillero56);
		lista6.add(casillero57);
		lista6.add(casillero58);
		lista6.add(casillero59);
		lista6.add(casillero60);
		
		columna6.setElementColum(lista6);
		
		lista7.add(casillero61);
		lista7.add(casillero62);
		lista7.add(casillero63);
		lista7.add(casillero64);
		lista7.add(casillero65);
		lista7.add(casillero66);
		lista7.add(casillero67);
		lista7.add(casillero68);
		lista7.add(casillero69);
		lista7.add(casillero70);
		
		columna7.setElementColum(lista7);
		
		lista8.add(casillero71);
		lista8.add(casillero72);
		lista8.add(casillero73);
		lista8.add(casillero74);
		lista8.add(casillero75);
		lista8.add(casillero76);
		lista8.add(casillero77);
		lista8.add(casillero78);
		lista8.add(casillero79);
		lista8.add(casillero80);
		
		columna8.setElementColum(lista8);
		
		lista9.add(casillero81);
		lista9.add(casillero82);
		lista9.add(casillero83);
		lista9.add(casillero84);
		lista9.add(casillero85);
		lista9.add(casillero86);
		lista9.add(casillero87);
		lista9.add(casillero88);
		lista9.add(casillero89);
		lista9.add(casillero90);
		
		columna9.setElementColum(lista9);
		
		lista10.add(casillero91);
		lista10.add(casillero92);
		lista10.add(casillero93);
		lista10.add(casillero94);
		lista10.add(casillero95);
		lista10.add(casillero96);
		lista10.add(casillero97);
		lista10.add(casillero98);
		lista10.add(casillero99);
		lista10.add(casillero100);
		
		columna10.setElementColum(lista10);
		
		filas.add(columna1);
		filas.add(columna2);
		filas.add(columna3);
		filas.add(columna4);
		filas.add(columna5);
		filas.add(columna6);
		filas.add(columna7);
		filas.add(columna8);
		filas.add(columna9);
		filas.add(columna10);
		
		laberinto.setElementFila(filas);

		return laberinto;
		
		
	}

    public Laberinto escenarioCuatro(){

    	Laberinto laberinto = new Laberinto();
		
		CasillerosLaberintoList columna1 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna2 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna3 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna4 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna5 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna6 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna7 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna8 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna9 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna10 = new CasillerosLaberintoList();
		
		List<CasillerosLaberintoList> filas = new ArrayList<CasillerosLaberintoList>();
		
		
		List<CasilleroLaberinto> lista1 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista2 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista3 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista4 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista5 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista6 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista7 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista8 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista9 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista10 = new ArrayList<CasilleroLaberinto>();
		
		CasilleroLaberinto casillero1 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero2 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero3 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero4 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero5 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero6 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero7 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero8 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero9 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero10 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero11 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero12 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero13 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero14 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero15 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero16 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero17 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero18 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero19 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero20 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero21 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero22 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero23 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero24 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero25 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero26 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero27 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero28 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero29 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero30 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero31 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero32 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero33 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero34 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero35 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero36 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero37 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero38 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero39 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero40 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero41 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero42 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero43 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero44 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero45 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero46 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero47 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero48 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero49 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero50 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero51 = new CasilleroLaberinto("e",true,true);
		CasilleroLaberinto casillero52 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero53 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero54 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero55 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero56 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero57 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero58 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero59 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero60 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero61 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero62 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero63 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero64 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero65 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero66 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero67 = new CasilleroLaberinto("g",false,false);
		CasilleroLaberinto casillero68 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero69 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero70 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero71 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero72 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero73 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero74 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero75 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero76 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero77 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero78 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero79 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero80 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero81 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero82 = new CasilleroLaberinto("k",false,true);
		CasilleroLaberinto casillero83 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero84 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero85 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero86 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero87 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero88 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero89 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero90 = new CasilleroLaberinto("s",false,false);
		
		CasilleroLaberinto casillero91 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero92 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero93 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero94 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero95 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero96 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero97 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero98 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero99 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero100 = new CasilleroLaberinto("p",false,false);
		
		lista1.add(casillero1);
		lista1.add(casillero2);
		lista1.add(casillero3);
		lista1.add(casillero4);
		lista1.add(casillero5);
		lista1.add(casillero6);
		lista1.add(casillero7);
		lista1.add(casillero8);
		lista1.add(casillero9);
		lista1.add(casillero10);
		
		columna1.setElementColum(lista1);
	
		
		lista2.add(casillero11);
		lista2.add(casillero12);
		lista2.add(casillero13);
		lista2.add(casillero14);
		lista2.add(casillero15);
		lista2.add(casillero16);
		lista2.add(casillero17);
		lista2.add(casillero18);
		lista2.add(casillero19);
		lista2.add(casillero20);
		
		columna2.setElementColum(lista2);
		
		lista3.add(casillero21);
		lista3.add(casillero22);
		lista3.add(casillero23);
		lista3.add(casillero24);
		lista3.add(casillero25);
		lista3.add(casillero26);
		lista3.add(casillero27);
		lista3.add(casillero28);
		lista3.add(casillero29);
		lista3.add(casillero30);
		
		columna3.setElementColum(lista3);
		
		lista4.add(casillero31);
		lista4.add(casillero32);
		lista4.add(casillero33);
		lista4.add(casillero34);
		lista4.add(casillero35);
		lista4.add(casillero36);
		lista4.add(casillero37);
		lista4.add(casillero38);
		lista4.add(casillero39);
		lista4.add(casillero40);
		
		columna4.setElementColum(lista4);
		
		lista5.add(casillero41);
		lista5.add(casillero42);
		lista5.add(casillero43);
		lista5.add(casillero44);
		lista5.add(casillero45);
		lista5.add(casillero46);
		lista5.add(casillero47);
		lista5.add(casillero48);
		lista5.add(casillero49);
		lista5.add(casillero50);
		
		columna5.setElementColum(lista5);
		
		lista6.add(casillero51);
		lista6.add(casillero52);
		lista6.add(casillero53);
		lista6.add(casillero54);
		lista6.add(casillero55);
		lista6.add(casillero56);
		lista6.add(casillero57);
		lista6.add(casillero58);
		lista6.add(casillero59);
		lista6.add(casillero60);
		
		columna6.setElementColum(lista6);
		
		lista7.add(casillero61);
		lista7.add(casillero62);
		lista7.add(casillero63);
		lista7.add(casillero64);
		lista7.add(casillero65);
		lista7.add(casillero66);
		lista7.add(casillero67);
		lista7.add(casillero68);
		lista7.add(casillero69);
		lista7.add(casillero70);
		
		columna7.setElementColum(lista7);
		
		lista8.add(casillero71);
		lista8.add(casillero72);
		lista8.add(casillero73);
		lista8.add(casillero74);
		lista8.add(casillero75);
		lista8.add(casillero76);
		lista8.add(casillero77);
		lista8.add(casillero78);
		lista8.add(casillero79);
		lista8.add(casillero80);
		
		columna8.setElementColum(lista8);
		
		lista9.add(casillero81);
		lista9.add(casillero82);
		lista9.add(casillero83);
		lista9.add(casillero84);
		lista9.add(casillero85);
		lista9.add(casillero86);
		lista9.add(casillero87);
		lista9.add(casillero88);
		lista9.add(casillero89);
		lista9.add(casillero90);
		
		columna9.setElementColum(lista9);
		
		lista10.add(casillero91);
		lista10.add(casillero92);
		lista10.add(casillero93);
		lista10.add(casillero94);
		lista10.add(casillero95);
		lista10.add(casillero96);
		lista10.add(casillero97);
		lista10.add(casillero98);
		lista10.add(casillero99);
		lista10.add(casillero100);
		
		columna10.setElementColum(lista10);
		
		filas.add(columna1);
		filas.add(columna2);
		filas.add(columna3);
		filas.add(columna4);
		filas.add(columna5);
		filas.add(columna6);
		filas.add(columna7);
		filas.add(columna8);
		filas.add(columna9);
		filas.add(columna10);
		
		laberinto.setElementFila(filas);

		return laberinto;
    	
    }

   public Laberinto escenarioCinco(){

		Laberinto laberinto = new Laberinto();
		
		CasillerosLaberintoList columna1 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna2 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna3 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna4 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna5 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna6 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna7 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna8 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna9 = new CasillerosLaberintoList();
		CasillerosLaberintoList columna10 = new CasillerosLaberintoList();
		
		List<CasillerosLaberintoList> filas = new ArrayList<CasillerosLaberintoList>();
		
		
		List<CasilleroLaberinto> lista1 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista2 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista3 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista4 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista5 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista6 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista7 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista8 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista9 = new ArrayList<CasilleroLaberinto>();
		List<CasilleroLaberinto> lista10 = new ArrayList<CasilleroLaberinto>();
		
		CasilleroLaberinto casillero1 = new CasilleroLaberinto("p",false,true);
		CasilleroLaberinto casillero2 = new CasilleroLaberinto("p",false,true);
		CasilleroLaberinto casillero3 = new CasilleroLaberinto("p",false,true);
		CasilleroLaberinto casillero4 = new CasilleroLaberinto("p",false,true);
		CasilleroLaberinto casillero5 = new CasilleroLaberinto("p",false,true);
		CasilleroLaberinto casillero6 = new CasilleroLaberinto("p",false,true);
		CasilleroLaberinto casillero7 = new CasilleroLaberinto("p",false,true);
		CasilleroLaberinto casillero8 = new CasilleroLaberinto("p",false,true);
		CasilleroLaberinto casillero9 = new CasilleroLaberinto("p",false,true);
		CasilleroLaberinto casillero10 = new CasilleroLaberinto("p",false,true);
		
		CasilleroLaberinto casillero11 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero12 = new CasilleroLaberinto("k",false,true);
		CasilleroLaberinto casillero13 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero14 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero15 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero16 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero17 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero18 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero19 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero20 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero21 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero22 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero23 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero24 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero25 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero26 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero27 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero28 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero29 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero30 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero31 = new CasilleroLaberinto("e",false,true);
		CasilleroLaberinto casillero32 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero33 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero34 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero35 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero36 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero37 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero38 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero39 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero40 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero41 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero42 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero43 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero44 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero45 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero46 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero47 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero48 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero49 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero50 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero51 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero52 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero53 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero54 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero55 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero56 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero57 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero58 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero59 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero60 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero61 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero62 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero63 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero64 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero65 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero66 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero67 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero68 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero69 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero70 = new CasilleroLaberinto("s",false,false);
		
		CasilleroLaberinto casillero71 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero72 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero73 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero74 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero75 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero76 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero77 = new CasilleroLaberinto("g",false,false);
		CasilleroLaberinto casillero78 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero79 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero80 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero81 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero82 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero83 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero84 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero85 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero86 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero87 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero88 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero89 = new CasilleroLaberinto("c",false,true);
		CasilleroLaberinto casillero90 = new CasilleroLaberinto("p",false,false);
		
		CasilleroLaberinto casillero91 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero92 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero93 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero94 = new CasilleroLaberinto("o",false,true);
		CasilleroLaberinto casillero95 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero96 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero97 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero98 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero99 = new CasilleroLaberinto("p",false,false);
		CasilleroLaberinto casillero100 = new CasilleroLaberinto("p",false,false);
		
		lista1.add(casillero1);
		lista1.add(casillero2);
		lista1.add(casillero3);
		lista1.add(casillero4);
		lista1.add(casillero5);
		lista1.add(casillero6);
		lista1.add(casillero7);
		lista1.add(casillero8);
		lista1.add(casillero9);
		lista1.add(casillero10);
		
		columna1.setElementColum(lista1);
	
		
		lista2.add(casillero11);
		lista2.add(casillero12);
		lista2.add(casillero13);
		lista2.add(casillero14);
		lista2.add(casillero15);
		lista2.add(casillero16);
		lista2.add(casillero17);
		lista2.add(casillero18);
		lista2.add(casillero19);
		lista2.add(casillero20);
		
		columna2.setElementColum(lista2);
		
		lista3.add(casillero21);
		lista3.add(casillero22);
		lista3.add(casillero23);
		lista3.add(casillero24);
		lista3.add(casillero25);
		lista3.add(casillero26);
		lista3.add(casillero27);
		lista3.add(casillero28);
		lista3.add(casillero29);
		lista3.add(casillero30);
		
		columna3.setElementColum(lista3);
		
		lista4.add(casillero31);
		lista4.add(casillero32);
		lista4.add(casillero33);
		lista4.add(casillero34);
		lista4.add(casillero35);
		lista4.add(casillero36);
		lista4.add(casillero37);
		lista4.add(casillero38);
		lista4.add(casillero39);
		lista4.add(casillero40);
		
		columna4.setElementColum(lista4);
		
		lista5.add(casillero41);
		lista5.add(casillero42);
		lista5.add(casillero43);
		lista5.add(casillero44);
		lista5.add(casillero45);
		lista5.add(casillero46);
		lista5.add(casillero47);
		lista5.add(casillero48);
		lista5.add(casillero49);
		lista5.add(casillero50);
		
		columna5.setElementColum(lista5);
		
		lista6.add(casillero51);
		lista6.add(casillero52);
		lista6.add(casillero53);
		lista6.add(casillero54);
		lista6.add(casillero55);
		lista6.add(casillero56);
		lista6.add(casillero57);
		lista6.add(casillero58);
		lista6.add(casillero59);
		lista6.add(casillero60);
		
		columna6.setElementColum(lista6);
		
		lista7.add(casillero61);
		lista7.add(casillero62);
		lista7.add(casillero63);
		lista7.add(casillero64);
		lista7.add(casillero65);
		lista7.add(casillero66);
		lista7.add(casillero67);
		lista7.add(casillero68);
		lista7.add(casillero69);
		lista7.add(casillero70);
		
		columna7.setElementColum(lista7);
		
		lista8.add(casillero71);
		lista8.add(casillero72);
		lista8.add(casillero73);
		lista8.add(casillero74);
		lista8.add(casillero75);
		lista8.add(casillero76);
		lista8.add(casillero77);
		lista8.add(casillero78);
		lista8.add(casillero79);
		lista8.add(casillero80);
		
		columna8.setElementColum(lista8);
		
		lista9.add(casillero81);
		lista9.add(casillero82);
		lista9.add(casillero83);
		lista9.add(casillero84);
		lista9.add(casillero85);
		lista9.add(casillero86);
		lista9.add(casillero87);
		lista9.add(casillero88);
		lista9.add(casillero89);
		lista9.add(casillero90);
		
		columna9.setElementColum(lista9);
		
		lista10.add(casillero91);
		lista10.add(casillero92);
		lista10.add(casillero93);
		lista10.add(casillero94);
		lista10.add(casillero95);
		lista10.add(casillero96);
		lista10.add(casillero97);
		lista10.add(casillero98);
		lista10.add(casillero99);
		lista10.add(casillero100);
		
		columna10.setElementColum(lista10);
		
		filas.add(columna1);
		filas.add(columna2);
		filas.add(columna3);
		filas.add(columna4);
		filas.add(columna5);
		filas.add(columna6);
		filas.add(columna7);
		filas.add(columna8);
		filas.add(columna9);
		filas.add(columna10);
		
		laberinto.setElementFila(filas);
		
		return laberinto;
		
	}
}
