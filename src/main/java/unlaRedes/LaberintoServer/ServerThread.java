package unlaRedes.LaberintoServer;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import javax.xml.bind.JAXBException;

import laberinto.Acciones;
import unlaRedes.LaberintoServer.enums.EstadoResponseEnum;
import unlaRedes.LaberintoServer.protocol.Estado;
import unlaRedes.LaberintoServer.protocol.LaberintoRequest;
import unlaRedes.LaberintoServer.protocol.LaberintoResponse;

/**
 * Se va a abrir un thread por cada cliente que se quiera conectar de esa forma
 * el server es concurrente
 * 
 * @author julián
 *
 */
public class ServerThread extends Thread {
	private Socket socket;

	public ServerThread(Socket socket) {
		super();
		this.socket = socket;
	}

	public void run() {
		DataOutputStream salida = null;
		BufferedReader entrada = null;
		LaberintoRequest laberinto = null;
		LaberintoResponse response = null;
		Estado estado = null;
		// Mensaje que recibe
		try {
			entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			String mensaje = entrada.readLine();
			System.out.println("Recibido: " + mensaje);

			try {
				laberinto = LaberintoRequest.unmarshal(mensaje);
				Acciones acciones = new Acciones();
				response = acciones.accion(laberinto);
			} catch (Exception e) {
				response = new LaberintoResponse();
				estado = new Estado();
				estado.setCodigoRespues(EstadoResponseEnum.PROTOCOLO_INVALIDO.getCodigo());
				estado.setDescripcion(EstadoResponseEnum.PROTOCOLO_INVALIDO.getDescripcion());
				response.setEstado(estado);
			}

			// Respuesta
			salida = new DataOutputStream(socket.getOutputStream());
			salida.writeBytes(LaberintoResponse.marshal(response));
		} catch (IOException | JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				entrada.close();
				salida.close();
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

}
