package unlaRedes.LaberintoServer.protocol;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * VEndria a ser el xml que le mandamos al server Cada atributo representa un
 * tag del xml, por ahora considere que va a haber una sola operacion, despues
 * habria que chequear si son mas de una entonces hay que armar otro xml o bien
 * modificar este
 * 
 * comando: va a ser un enum supongo tomaria valores como -UP,DW,RI,LE para
 * movimientos y hay que pensar alguno como pagar al guardia y otro para
 * levantar el oro y la key
 * 
 * elementFila -> El laberinto en si tiene es una lista de listas en este nivel
 * es la fila y dentro de CasillerosLaberintoList las columnas (si se les ocurre
 * algo mejor modifiquen)
 * 
 * Falta ver el tema del logueo
 * 
 * @author julián
 *
 */
@XmlRootElement(name="laberintoRequest")
@XmlType(propOrder = { "login", "comando", "jugador", "laberinto" })
public class LaberintoRequest {
	
	private Login login;
	
	private String comando;
	
	private Jugador jugador;
	
	private Laberinto laberinto;

	@XmlElement(name="login")
	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	@XmlElement(name="comando")
	public String getComando() {
		return comando;
	}

	public void setComando(String comando) {
		this.comando = comando;
	}

	@XmlElement(name="jugador")
	public Jugador getJugador() {
		return jugador;
	}

	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}

	@XmlElement(name="laberinto")
	public Laberinto getLaberinto() {
		return laberinto;
	}

	public void setLaberinto(Laberinto laberinto) {
		this.laberinto = laberinto;
	}

	/**
	 * Transforma el xml a Objeto
	 * @param xml
	 * @return
	 * @throws JAXBException
	 */
	public static LaberintoRequest unmarshal (String xml) throws JAXBException {
		
		JAXBContext jaxbContext = JAXBContext.newInstance(LaberintoRequest.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		return (LaberintoRequest) jaxbUnmarshaller.unmarshal(reader);
	}
	
	public static String marshal (LaberintoRequest laberintoRequest) throws JAXBException {
		StringWriter sw = new StringWriter();
		JAXBContext context = JAXBContext.newInstance(LaberintoRequest.class);
		Marshaller m = context.createMarshaller();

		m.marshal(laberintoRequest, sw);
		
		return sw.toString();
	}
}
