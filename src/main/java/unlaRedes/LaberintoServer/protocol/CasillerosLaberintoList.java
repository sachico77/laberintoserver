package unlaRedes.LaberintoServer.protocol;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;


public class CasillerosLaberintoList {

	
	private List<CasilleroLaberinto> elementColum;

	@XmlElement(name="elementColum")
	public List<CasilleroLaberinto> getElementColum() {
		return elementColum;
	}

	public void setElementColum(List<CasilleroLaberinto> elementColum) {
		this.elementColum = elementColum;
	}
	
	
}
