package unlaRedes.LaberintoServer.protocol;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Laberinto {
	
	private List<CasillerosLaberintoList> elementFila;
	private boolean finalizado;
	private int numero;
	
	
	public Laberinto() {
		super();
		finalizado = false;
	}

	@XmlElement(name="numero")
	public int getNumero(){
	 return numero;
	}
	
	public void setNumero(int numero){
		this.numero=numero;
	}
	@XmlElement(name="elementFila")
	public List<CasillerosLaberintoList> getElementFila() {
		return elementFila;
	}

	public void setElementFila(List<CasillerosLaberintoList> elementFila) {
		this.elementFila = elementFila;
	}
	
	@XmlElement(name="finalizado")
	public boolean isFinalizado() {
		return finalizado;
	}

	public void setFinalizado(boolean finalizado) {
		this.finalizado = finalizado;
	}

}
