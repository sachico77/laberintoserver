package unlaRedes.LaberintoServer.protocol;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "codigoRespues", "descripcion" })

public class Estado {

	private String codigoRespues;
	private String descripcion;

	@XmlElement(name="codigoRespues")
	public String getCodigoRespues() {
		return codigoRespues;
	}

	public void setCodigoRespues(String codigoRespues) {
		this.codigoRespues = codigoRespues;
	}

	@XmlElement(name="descripcion")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
