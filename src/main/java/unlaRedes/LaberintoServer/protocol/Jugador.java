package unlaRedes.LaberintoServer.protocol;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * posicionFila -> posicion actual/a la que va a pasar en la fila
 * posicionColumna -> posicion actual/a la que va a pasar en la fila (Tipo la
 * batalla naval)
 * 
 * poseeKey-> para saber si la tiene
 * 
 * pasaGuardia -> La idea es que en si se encuentra con el guardia tiene oro
 * suficiente para pasarlo (hay que chequear si esto lo vamos a usar)
 * 
 * @author julián
 *
 */
@XmlType(propOrder = { "posicionFila", "posicionColumna", "poseeKey", "oro","pasaGuarda" })
public class Jugador {


	private Integer posicionFila;

	private Integer posicionColumna;

	private boolean poseeKey;

	private int oro;

	private boolean pasaGuarda;

	public Jugador() {
		super();
		oro = 0;
		poseeKey = false;
		pasaGuarda = false;
	}

	public Jugador(Integer posicionFila, Integer posicionColumna, boolean poseeKey, int oro, boolean pasaGuarda) {
		super();
		this.posicionFila = posicionFila;
		this.posicionColumna = posicionColumna;
		this.poseeKey = poseeKey;
		this.oro = oro;
		this.pasaGuarda = pasaGuarda;
	}
	
	@XmlElement(name="posicionFila")
	public Integer getPosicionFila() {
		return posicionFila;
	}

	public void setPosicionFila(Integer posicionFila) {
		this.posicionFila = posicionFila;
	}
	
	@XmlElement(name="posicionColumna")
	public Integer getPosicionColumna() {
		return posicionColumna;
	}

	public void setPosicionColumna(Integer posicionColumna) {
		this.posicionColumna = posicionColumna;
	}

	@XmlElement(name="poseeKey")
	public boolean isPoseeKey() {
		return poseeKey;
	}

	public void setPoseeKey(boolean poseeKey) {
		this.poseeKey = poseeKey;
	}

	@XmlElement(name="oro")
	public int getOro() {
		return oro;
	}

	public void setOro(int oro) {
		this.oro = oro;
	}

	@XmlElement(name="pasaGuarda")
	public boolean isPasaGuarda() {
		return pasaGuarda;
	}

	public void setPasaGuarda(boolean pasaGuarda) {
		this.pasaGuarda = pasaGuarda;
	}

}
