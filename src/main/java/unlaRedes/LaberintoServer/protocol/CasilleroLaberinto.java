package unlaRedes.LaberintoServer.protocol;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Mostrar: va a estar tambien en el response es lo que vamos a usar para saber
 * si esa parte del laberinto se muestra o no
 * 
 * Elemento: va a ser un enum Basicamente podria tomar los valores: - K -> Key -
 * O -> oro - G -> Guardia - C -> Camino - P -> Pared - E -> Entrada puedePasar:
 * indica si el jugador puede pasar atravez del elemnto - E,k,C,O, G(teniendo
 * oro) -> true - P, G(sin oro) -> false
 * 
 * @author julián
 *
 */
@XmlType(propOrder = { "elemento", "mostrar", "puedePasar" })
public class CasilleroLaberinto {

	private String elemento;

	private boolean mostrar;

	private boolean puedePasar;

	public CasilleroLaberinto() {
		super();
		mostrar = false;
	}

	public CasilleroLaberinto(String elemento, boolean mostrar, boolean puedePasar) {

		this.elemento = elemento;
		this.mostrar = mostrar;
		this.puedePasar = puedePasar;

	}

	@XmlElement(name="elemento")
	public String getElemento() {
		return elemento;
	}

	public void setElemento(String elemento) {
		this.elemento = elemento;
	}

	@XmlElement(name="mostrar")
	public boolean isMostrar() {
		return mostrar;
	}

	public void setMostrar(boolean mostrar) {
		this.mostrar = mostrar;
	}

	@XmlElement(name="puedePasar")
	public boolean isPuedePasar() {
		return puedePasar;
	}

	public void setPuedePasar(boolean puedePasar) {
		this.puedePasar = puedePasar;
	}

}
