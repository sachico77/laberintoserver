package unlaRedes.LaberintoServer.protocol;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Vendria a Ser el Xml que responde el server
 * 
 * EnJugador lo que va a cambiar respecto del request es que el server nos va a
 * indicar si esta habilitado o no para pasar al guardia y la nueva posicion del
 * 
 * elementFila: el server va a actualizar lo que vemos del laberinto
 * 
 * estado: un estado general si por ejemplo el movimiento fue valido o no, si
 * hubo algun tipo de error etc. jugador Falta ver el tema del logueo
 * 
 * @author julián
 *
 */
@XmlRootElement(name="laberintoResponse")
@XmlType(propOrder = { "estado", "jugador", "laberinto", "login" })
public class LaberintoResponse {
	private Estado estado;
	private Jugador jugador;
	private Laberinto laberinto;
	private Login login;

	@XmlElement(name="estado")
	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	@XmlElement(name="jugador")
	public Jugador getJugador() {
		return jugador;
	}

	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}

	@XmlElement(name="laberinto")
	public Laberinto getLaberinto() {
		return laberinto;
	}

	public void setLaberinto(Laberinto laberinto) {
		this.laberinto = laberinto;
	}
	
	
	@XmlElement(name="login")
	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	/**
	 * Transforma el xml a Objeto
	 * @param xml
	 * @return
	 * @throws JAXBException
	 */
	public static LaberintoResponse unmarshal (String xml) throws JAXBException {
		
		JAXBContext jaxbContext = JAXBContext.newInstance(LaberintoResponse.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		return (LaberintoResponse) jaxbUnmarshaller.unmarshal(reader);
	}
	
	public static String marshal (LaberintoResponse laberintoResponse) throws JAXBException {
		StringWriter sw = new StringWriter();
		JAXBContext context = JAXBContext.newInstance(LaberintoResponse.class);
		Marshaller m = context.createMarshaller();

		m.marshal(laberintoResponse, sw);
		
		return sw.toString();
	}

}
