package unlaRedes.LaberintoServer.enums;

public enum ComandosEnum {

	ARRIBA("AR"),
	ABAJO("AB"),
	IZQUIERDA("IZ"),
	DERECHA("DE"),
	INICIARLAB("IN"),
	LOGIN("LO");
	
	private String comando;

	private ComandosEnum(String comando) {
		this.comando = comando;
	}

	public String getComando() {
		return comando;
	}
	
	public static ComandosEnum encontrarValue(String comando){
	    for(ComandosEnum v : values()){
	        if( v.getComando().equals(comando)){
	            return v;
	        }
	    }
	    return null;
	}
	
}
