package unlaRedes.LaberintoServer.enums;

public enum ElementosEnum {

	LLAVE("K"),
	PARED("P"),
	CAMINO("C"),
	ORO("O"),
	GUARDIA("G"),
	SALIDA("S"),
	ENTRADA("E");
	
	private String elemento;

	private ElementosEnum(String elemento) {
		this.elemento = elemento;
	}

	public String getElemento() {
		return elemento;
	}
	
	public static ElementosEnum encontrarValue(String elemento){
	    for(ElementosEnum v : values()){
	        if( v.getElemento().equals(elemento)){
	            return v;
	        }
	    }
	    return null;
	}
}
