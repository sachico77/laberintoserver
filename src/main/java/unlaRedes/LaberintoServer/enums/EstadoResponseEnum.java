package unlaRedes.LaberintoServer.enums;

public enum EstadoResponseEnum {

	Exito("200", "OK"),
	LOG_EXITOS("203","USUARIO LOGGEADO CON EXITO"),
	ATRAPADO("202", "TE ATRAPO EL GUARDIA, YOU LOSE!"),
	MOV_INVALIDO("300", "MOVIMIENTO INVALIDO"),
	NO_SE_RECONOCE_COMANDO("301","NO SE RECONOCE COMANDO"),
	USUARIO_INVALIDO("302","USUARIO INVALIDO"),
	CONTRASENA_INVALIDA("303", "CONTRASENA INVALIDA"),
	FALTA_LLAVE("304", "NECESITA LA LLAVE"),
	NO_EXISTE_LAB("305", "No existe laberinto elegido"),
	PROTOCOLO_INVALIDO("306", "PROTOCOLO INVALIDO");
	
	private String codigo;
	private String descripcion;
	
	private EstadoResponseEnum(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	
	public static EstadoResponseEnum encontrarValue(String codigo){
	    for(EstadoResponseEnum v : values()){
	        if( v.getCodigo().equals(codigo)){
	            return v;
	        }
	    }
	    return null;
	}
}
