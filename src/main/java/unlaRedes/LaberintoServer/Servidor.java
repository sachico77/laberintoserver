package unlaRedes.LaberintoServer;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import unlaRedes.LaberintoServer.protocol.LaberintoRequest;

public class Servidor {

	public static void main(String[] args) throws Exception {

		ServerSocket server;
		int puerto = 9000;
		server = new ServerSocket(puerto);
		
		try {

			// Server siempre queda corriendo
			while (true) {
				System.out.println("servidor esperando cliente");
				// esperando a que se mande una peticion del Cliente A traves del puerto 9000
				new ServerThread(server.accept()).start();
			}

		} catch (Exception e) {
		} finally {
			try {
				server.close();
			} catch (Exception e) {
			}
		}
		;

	}

}
